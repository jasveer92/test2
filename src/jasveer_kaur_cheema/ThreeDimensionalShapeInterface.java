package jasveer_kaur_cheema;

public interface ThreeDimensionalShapeInterface {
	public double calculateVolume();
	public double printDetails();
}
