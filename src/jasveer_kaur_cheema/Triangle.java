package jasveer_kaur_cheema;

public class Triangle extends Shape {
	
	
	 // Private member variables
	   private int base;
	   private int height;

	   // Constructor
	   public Triangle(int base, int height) {
	      this.base = base;
	      this.height = height;
	   }

	   @Override
	   public String toString() {
	      return "Triangle[base=" + base + ",height=" + height + "]";
	   }

	   // Need to implement all the abstract methods defined in the interface
	   @Override
	   public double Area() {
	      return 0.5 * base * height;
	   }
	}
  
    


